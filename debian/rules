#!/usr/bin/make -f
SHELL=/bin/bash -e

CFLAGS=-g -O2
DEB_BUILD_MAINT_OPTIONS=hardening=+pie,+bindnow
-include /usr/share/dpkg/buildflags.mk

QUILT_STAMPFN := debian/.stamp-patched
include /usr/share/quilt/quilt.make

B := $(CURDIR)/build
D := $(CURDIR)/debian/inn

# get the debian revision number
DREV := $(shell dpkg-parsechangelog | grep '^Version: 1:1.7.2q-' \
	| sed -e 's/^.*-\(.*\)$$/\1/')
ifeq ($(DREV),)
$(error bad package version)
endif

# the upstream source needs to be copied in the build dir
src_files = $(shell find . -maxdepth 1 \
	-not -name . -and -not -name debian -and -not -name .pc \
	-and -not -name extra -and -not -name build)

clean: unpatch
	rm -rf debian/.stamp-* $B
	dh_clean

configure: debian/.stamp-configure
debian/.stamp-configure: $(QUILT_STAMPFN)
	dh_testdir
	mkdir -p $B
	for dir in $(src_files); do cp -ldpR $$dir $B; done
	perl -pi -e 's|^(#define\s+LOCAL_STRING\s+")[^"]*"|$${1}Debian/$(DREV)"|' \
		$B/include/patchlevel.h
	sed -e "s@PERL_LDOPTS@`perl extra/perl-ldopts`@" \
	    -e "s@PERL_CCOPTS@`perl extra/perl-ccopts`@" \
	    -e "s@DEBIAN_CFLAGS@$(CFLAGS)@" \
	    -e "s@DEBIAN_CPPFLAGS@$(CPPFLAGS)@" \
	    -e "s@DEBIAN_LDFLAGS@$(LDFLAGS)@" \
	    < extra/config.data.debian > $B/config/config.data
	touch $@

build: build-arch build-indep
build-arch: debian/.stamp-build
build-indep: debian/.stamp-build
debian/.stamp-build: debian/.stamp-configure
	dh_testdir
	cd $B && $(MAKE)
	# Cat the Install docs together
	cat $B/Install.ms.[12] | nroff -ms > $B/Install.txt
	touch $@

binary-arch: debian/.stamp-build checkroot
	dh_testdir
	dh_clean
	dh_installdirs
	# Install INN in the temp directory
	cd $B && $(MAKE) install DESTDIR=$D

	printf '#!/bin/sh\ngzip -d\n' > $D/usr/lib/news/rnews/gunbatch
	printf '#!/bin/sh\nxz -d\n'   > $D/usr/lib/news/rnews/xunbatch
	chmod +x $D/usr/lib/news/rnews/*unbatch

	cp extra/innreport_inn.pm $D/usr/lib/news/
	install -m 755 extra/send-uucp.pl extra/innreport $D/usr/lib/news/bin/
	pod2man --section=8 extra/send-uucp.pl \
		> $D/usr/share/man/man8/send-uucp.8

	# these scripts contain an unfixable bashism
	cd $D/usr/lib/news/control/ && \
	for file in *; do \
		perl -i -pe 's@^#! /bin/sh@#! /bin/bash@' $$file; \
	done

	# Fix up innshellvars* and other things
	perl -pi -e 's#/usr/ucb#/usr/sbin#' $D/usr/lib/news/innshellvars*

	cp extra/etc/* $D/etc/news
	dh_installcron
	dh_installdocs extra/doc/inn-README extra/doc/innhelp.tomk.txt \
		$B/FAQ/ $B/Install.txt $B/actived/README.actived
	dh_installchangelogs $B/CHANGES
	dh_installinit
	dh_installsystemd
	install -p -m755 debian/inn.init $D/etc/init.d/inn

	rm -rf	$D/var/spool/news/* $D/run/ $D/tmp/ \
		$D/etc/news/scripts/*.tcl \
		$D/var/lib/news/innlog.pl \
		$D/usr/share/man/man8/innlog.pl.8.gz \
		$D/usr/lib/news/rnews/c7unbatch \
		$D/usr/lib/news/bin/innmail \
		$D/usr/lib/news/control/sample.control

	dh_fixperms
	cd $D/etc/news && \
		chown root:news passwd.nntp nnrp.access hosts.nntp && \
		chmod 640 passwd.nntp nnrp.access hosts.nntp
	chown root:news $D/usr/bin/[ri]news
	chmod 2755 $D/usr/bin/[ri]news
	chown root:news $D/usr/sbin/inndstart
	chmod 4754 $D/usr/sbin/inndstart
	chown -R news:news $D/var/*/news/

	# Move some more stuff around.
	mv $D/var/lib/news/send-* $D/etc/news/scripts/
	mv $D/var/lib/news/nntpsend.ctl $D/etc/news/nntpsend.ctl
	cd $D/usr/share/man/man8 && mv nnrpd.8 in.nnrpd.8

	# Install the latest pgpverify.
	install -m 755 extra/gpgverify $D/usr/lib/news/bin/pgpverify
	install -d -m 755 -o news -g news $D/etc/news/pgp
	install -m 644 extra/pgpverify.8 $D/usr/share/man/man8/

	# Install controlchan
	install -m 755 extra/cch/controlchan $D/usr/lib/news/bin/
	install -m 644 extra/cch/control/* $D/usr/lib/news/control/

	dh_link
	dh_strip
	dh_compress
	dh_installdeb
	dh_shlibdeps
	dh_perl
	dh_gencontrol -u-VPERLAPI=$$(perl -MConfig -e 'print "perlapi-" . ($$Config{debian_abi} || $$Config{version})')
	dh_builddeb

binary-indep:

binary: binary-arch

checkroot:
	test root = "`whoami`"

